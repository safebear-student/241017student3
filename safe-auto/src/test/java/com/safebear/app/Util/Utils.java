package com.safebear.app.Util;

import org.openqa.selenium.WebDriver;

public class Utils {
    WebDriver driver;
    String url;

    public boolean navigateToWebsite(WebDriver driver) {
        this.driver = driver;
        url = "http://automate.safebear.co.uk/";
        driver.get(url);
        return driver.getTitle().startsWith("Welcome");
    }
}

